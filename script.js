// Find users with letter 'a' in their first or last name.


db.users.find(
    {
        $or: [
            {"firstName": {$regex: "a", $options: "i"}},
            {"lastName": {$regex: "a", $options: "i"}}
        ]
    },
    {
        "firstName": 1,
        "lastName": 1,
        "_id": 0
    }
)


// Find the users who are admins and is active.

db.users.find(
    {
        $and: [
            {"isActive": true },
            {"isAdmin": true}
        ]
    }
)

// Find the courses with letter u in its name and has a price greater than or equal to 13000

db.courses.find(
    {
       "name": {$regex: "u", $options: "i"},
       "price": {$gte: "13000"}
    }
)